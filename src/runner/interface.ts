export interface IMailer {
    readonly recipient: string | "fixed";
    readonly recipientFixed?: string;
    readonly sender?: string | "fixed";
    readonly senderFixed?: string;
    readonly includeData?: boolean;
}
export interface IMailerCondition {
    readonly willMail: boolean;
}
